#Run with current environment (-V) and in the current directory (-cwd)
#$ -V -cwd

#Request some time- min 1 min - max 48 hours
#$ -l h_rt=00:20:00

#$ -l coproc_k80=2

#Get email at start and end of the job
#$ -m be

#Now run the job
module add cuda
./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/1.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/1.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/1.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/1.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/1.jpg

./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/2.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/2.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/2.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/2.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/2.jpg

./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/3.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/3.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/3.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/3.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/3.jpg

./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/4.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/4.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/4.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/4.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/4.jpg

./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/5.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/5.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/5.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/5.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/5.jpg

./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/6.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/6.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/6.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/6.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/6.jpg

./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/7.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/7.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/7.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/7.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/7.jpg

./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/8.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/8.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/8.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/8.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/8.jpg

./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/9.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/9.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/9.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/9.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e1/og/9.jpg

