#Run with current environment (-V) and in the current directory (-cwd)
#$ -V -cwd

#Request some time- min 1 min - max 48 hours
#$ -l h_rt=00:20:00

#$ -l coproc_p100=4

#Get email at start and end of the job
#$ -m be

#Now run the job
module add cuda 

./darknet detect cfg/yolov3.cfg yolov3.weights e2/11.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/11.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/11.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/11.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/11.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/12.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/12.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/12.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/12.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/12.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/13.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/13.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/13.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/13.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/13.jpg

./darknet detect cfg/yolov3.cfg yolov3.weights e2/21.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/21.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/21.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/21.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/21.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/22.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/22.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/22.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/22.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/22.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/23.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/23.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/23.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/23.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/23.jpg

./darknet detect cfg/yolov3.cfg yolov3.weights e2/31.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/31.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/31.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/31.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/31.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/32.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/32.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/32.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/32.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/32.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/33.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/33.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/33.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/33.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/33.jpg

./darknet detect cfg/yolov3.cfg yolov3.weights e2/41.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/41.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/41.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/41.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/41.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/42.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/42.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/42.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/42.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/42.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/43.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/43.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/43.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/43.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/43.jpg

./darknet detect cfg/yolov3.cfg yolov3.weights e2/51.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/51.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/51.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/51.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/51.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/52.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/52.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/52.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/52.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/52.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/53.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/53.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/53.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/53.jpg
./darknet detect cfg/yolov3.cfg yolov3.weights e2/53.jpg