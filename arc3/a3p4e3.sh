#Run with current environment (-V) and in the current directory (-cwd)
#$ -V -cwd

#Request some time- min 1 min - max 48 hours
#$ -l h_rt=00:20:00

#$ -l coproc_p100=4

#Get email at start and end of the job
#$ -m be

#Now run the job
module add cuda 

./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/1.jpg
./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/1.jpg
./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/1.jpg
./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/1.jpg
./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/1.jpg

./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/2.jpg
./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/2.jpg
./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/2.jpg
./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/2.jpg
./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/2.jpg

./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/3.jpg
./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/3.jpg
./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/3.jpg
./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/3.jpg
./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/3.jpg

./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/4.jpg
./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/4.jpg
./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/4.jpg
./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/4.jpg
./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/4.jpg

./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/5.jpg
./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/5.jpg
./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/5.jpg
./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/5.jpg
./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/5.jpg

./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/6.jpg
./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/6.jpg
./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/6.jpg
./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/6.jpg
./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/6.jpg

./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/7.jpg
./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/7.jpg
./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/7.jpg
./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/7.jpg
./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/7.jpg

./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/8.jpg
./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/8.jpg
./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/8.jpg
./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/8.jpg
./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/8.jpg

./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/9.jpg
./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/9.jpg
./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/9.jpg
./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/9.jpg
./darknet detect cfg/yolov3-tiny.cfg yolov3-tiny.weights e3/9.jpg

