#Run with current environment (-V) and in the current directory (-cwd)
#$ -V -cwd

#Request some time- min 10 min - max 48 hours
#$ -l h_rt=00:10:00

#$ -l coproc_v100=1

#Get email at start and end of the job
#$ -m be

#Now run the job
module add cuda opencv
./darknet detector demo cfg/coco.data cfg/yolov3.cfg yolov3.weights video/2.mp4
